"""
Pokerandy 2.0

NEW! IMPROVED! PYTHONIC!

includes some helper stuff stolen from https://github.com/kanzure/pokemon-reverse-engineering-tools
"""
from chars import chars
from pokedex import pokedex
from maps import maps


import copy
import curses
import json
import math
import os
import random
import re
import string
import sys
import time
import threading

import vba_wrapper 

import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket

class Application(tornado.web.Application):
	def __init__(self):
		handlers = [
			(r"/socket", PokeSocketHandler)
			]
		tornado.web.Application.__init__(self, handlers)


class PokeSocketHandler(tornado.websocket.WebSocketHandler):
	def open(self):
		if self not in wss:
			wss.append(self)
		time_calc = time_count + (time.time() - time_origin)
		statedump = {'rtc':int(time_calc)}
		self.write_message("STATEDUMP|%s|%s" % (json.dumps(statedump), key_count))

	def on_message(self, message):
		pass
	
	def on_close(self):
		if self in wss:
			wss.remove(self)




regex = re.compile(" [a-z]+ ",re.IGNORECASE)

# makes start a bit less common
less_start = [1,1,2,2,3,3,4,4,5,5,6,6,7]

twitch_prob = [[1,0.145],[2,0.14],[3,0.156],[4,0.186],[5,0.149],[6,0.144],[7,0.08]]


#pathfind_prob = [[1,0.157],[2,0.157],[3,0.157],[4,0.157],[5,0.149],[6,0.144],[7,0.08]]
pathfind_prob = [[1,0.145],[2,0.14],[3,0.156],[4,0.186],[5,0.149],[6,0.144],[7,0.08]]
PATH_PENALTY = 0.01
pathfind_default = copy.deepcopy(pathfind_prob)


class Rectangle(object):
	tlX = None
	tlY = None
	brX = None
	brY = None
	extend_dir = 0 # 1: up, 2: right, 3: down, 4: left
	dir_weights = {1:0, 2:0, 3:0, 4:0}	
	start_key = -1
	
	def __str__(self):
		return "[%s, %s] | [%s, %s]" % (self.tlX, self.tlY, self.brX, self.brY)

	def __init__(self, x1, y1, x2, y2):
		self.tlX, self.tlY, self.brX, self.brY = x1, y1, x2, y2
		self.dir_weights = {1:0, 2:0, 3:0, 4:0}
		self.start_key = -1

	def width(self):
		return self.brX - self.tlX

	def height(self):
		return self.brY - self.tlY


	def area_of_map_bounds(self, bounds):
		try:
			return (self.width() * self.height()) / float(((bounds[0]-4) * (bounds[1]-4)))
		except:
			raise

	def random_distr(self, l):
	    assert l # don't accept empty lists
	    r = random.uniform(0, 1)
	    s = 0
	    for i in xrange(len(l)):

		item, prob = l[i]
		s += prob
		if s >= r:
			return item
	    return item

	def pick_point_in_last_dir(self,new_map=False):
		self.dir_probs = []
		"""
		for i in xrange(1,5):
			if i == self.extend_dir:
				dir_probs.append([i,0.7])
			else:
				dir_probs.append([i,0.1])
		"""
		
		# weight for different directions
		#prob_sums = 0
		#for i in xrange(1,5):
		#	prob_sums += self.dir_weights[i]

		#for i in xrange(1,5):
		#	self.dir_probs.append([i-1, self.dir_weights[i] / float(prob_sums)]) 

		#random_extend = self.random_distr(self.dir_probs)
		#random_extend += 1


		# pick the dominant direction

		global last_key
		
		last_map = {1:4,2:2,3:3,4:1}
		last_key = map_change_key

		# excludes the opposite direction from last_key so we don't immediately turn around
		reverse_map = {1:3,2:4,3:1,4:2}
		clean_map = [1,2,3,4]
		clean_map.remove(reverse_map[last_key])

		if self.extend_dir == 0 and last_key != -1:# and (last_key < 5 or self.start_key != -1): #and last_key < 5:
			random_extend = random.choice(clean_map) 
			"""   # set dir based on where came from
			if self.start_key == -1:
				self.start_key = last_key
			random_extend = last_map[self.start_key]
			"""

		else:
			random_extend = self.keywithmaxval(self.dir_weights)	

		if random_extend== 1:
			point = random.randint(0, self.width())
			return [self.tlX+point, self.tlY-1]

		elif random_extend == 2:
			point = random.randint(0, self.height())
			return [self.brX+1, self.tlY+point]

		elif random_extend == 3:
			point = random.randint(0, self.width())
			return [self.tlX+point, self.brY+1]

		elif random_extend == 4:
			point = random.randint(0, self.height())
			return [self.tlX-1, self.tlY+point]

	def keywithmaxval(self,d):
	     """ a) create a list of the dict's keys and values; 
		 b) return the key with the max value"""  
	     v=list(d.values())
	     k=list(d.keys())
	     return k[v.index(max(v))]

	def pick_point_on_bounds(self,new_map=False):

		#print "w: %s | h: %s" % (self.width(), self.height())
		if(self.width() == 0 or self.height() == 0):
			return [self.brX, self.brY]

		# uncomment if not handling map transitions
		#if(self.extend_dir != 0):
		return self.pick_point_in_last_dir(new_map)

		point = random.randint(1,self.width()*2 + self.height()*2)
		#print "point: %s" % point

		if point < self.width():
			return [self.tlX + point, self.tlY]

		elif(point < (self.width()+self.height())):
			return [self.brX, self.tlY+(point-self.width())]

		elif(point < (2*self.width() + self.height())):
			return [(self.brX - (point-(self.width()+self.height()))), self.brY]

		else:
			return [self.tlX, self.brY - (point-((self.width()*2)+self.height()))]


	def point_in_rect(self, x, y):
		if((x >= self.tlX and x <= self.brX) and (y >= self.tlY and y <= self.brY)):
			return True
		else:
			return False
	"""
	Extends this rectangle to include the point x, y
	If the point is already in the rectangle, nothing happens
	"""
	def extend_to_point(self, x, y):
		if((x >= self.tlX and x <= self.brX) and (y >= self.tlY and y <= self.brY)):
			return

		if(x < self.tlX):
			self.extend_dir = 4
			self.dir_weights[4] += 1
			self.tlX = x

		if(x > self.brX):
			self.extend_dir = 2
			self.dir_weights[2] += 1
			self.brX = x
	
		if(y < self.tlY):
			self.extend_dir = 1
			self.dir_weights[1] += 1
			self.tlY = y

		if(y > self.brY):
			self.extend_dir = 3
			self.dir_weights[3] += 1
			self.brY = y
	
			
		#print "extended bounds to [(%s, %s), (%s, %s)]" % (self.tlX, self.tlY, self.brX, self.brY)

class Distributions:
	"""
	LessStart - d-pad and A/B equal, but Start has half the probability of occurring
	OverallTwitch - same button distribution as Twitch overall
	LiveTwitch - TODO: live key presses from Twitch
	DumbPathFind- peek at RAM to detect collisions and try to avoid them
	RandomGoal - identify a random target on the current map, and adjust distribution to go towards it, then change goal
	ProgressiveGoal - tracks visited locations and sets a goal on the periphery of this range
	NoChaosGoals - ProgressiveGoals without any chaos
	"""

	LessStart,OverallTwitch, DumbPathFind, RandomGoal, ProgressiveGoal, NoChaosGoals = range(6)

DISTRIBUTION = Distributions.NoChaosGoals

key_maps = {1:"l",2:"r",3:"d",4:"u",5:"a",6:"b",7:"start"}
friendly_maps = {1:"Left",2:"Right",3:"Down",4:"Up",5:"A",6:"B",7:"Start"}

vba = vba_wrapper.VBA("/home/lhutton/emu/pokeblue.gb")

GOAL_PERIOD = 15
DEFAULT_GOAL_PERIOD = 15

# states
last_is_in_battle = False
def is_in_battle():
	if(vba.read_memory_at(0xd057) == 0):
		return False
	else:
		return True


# goals
current_goal = None

# how many tiles to extend space into virtually for assigning goals
# this needs to be an even number to avoid ambiguous badness
GOAL_EXTENSION = 4

# multiples goal length to decide length of meta goal
META_MULTIPLIER = 5

# the most coverage we've had on this map. if this stops growing start assigning random goals in case we're stuck somewhere
meta_goal = -1
# keep assigning new random goals for the remainder of the meta period, if triggered
meta_active = False

def set_new_goal(force=False, new_map=False):
	write_time()

	

	if (DISTRIBUTION == Distributions.ProgressiveGoal or DISTRIBUTION == Distributions.NoChaosGoals) and not force and not meta_active:
		new_progressive_goal(new_map)
		return

	if not meta_active and bounding_rect.extend_dir == 0:
		new_progressive_goal(new_map)
		return

	global current_goal, last_key, min_goal_distance, pathfind_prob, map_bounds
	#pathfind_prob = copy.deepcopy(pathfind_default) #reset probs to default
	min_goal_distance = -1
	
	map_bounds = [vba.read_memory_at(0xd369)*2,vba.read_memory_at(0xd368)*2]
	
	# extend the bounds to encourage goals just outside the boundaries (and encourage us to take connections)
	map_bounds = [map_bounds[0]+GOAL_EXTENSION, map_bounds[1]+GOAL_EXTENSION]

	current_goal = [random.randint(0,map_bounds[0]),random.randint(0,map_bounds[1])]
	#send_socket("New goal at %s" % current_goal)
	send_goal_event("I'm lost, so I'm going to try heading %s.")


	#print "New goal at %s. Currently at %s,%s" % (current_goal, this_x, this_y)
	update_goal_paths(True)

bounding_rect = None
BOUNDING_THRESHOLD = 0.9 # how much of the map area needs to be covered before we get stop making progressive goals?
def new_progressive_goal(new_map=False):
	global last_key, current_goal, min_goal_distance, pathfind_prob
	
	# if not outside don't try to make progressive goals
	#if(last_tileset != 0 and last_tileset != 3):
	#	set_new_goal(True)
	#	return

	#if(map_bounds): 
		#if((bounding_rect.width() * bounding_rect.height()) >= ((map_bounds[0] * map_bounds[1]) * BOUNDING_THRESHOLD)):
		
		# debug: uncomment to start making random goals as map gets explored more
		#if(bounding_rect.area_of_map_bounds(map_bounds) >= 0.7):
		#	#print "Bounding box too big - start making random goals"
		#	set_new_goal(True)
		#	return
		
		#print "map bounds: %s" % map_bounds	
		#print "Bounding box is %s of map bounds" % bounding_rect.area_of_map_bounds(map_bounds)
	
	pathfind_prob = copy.deepcopy(pathfind_default)
	min_goal_distance = -1

	current_goal = bounding_rect.pick_point_on_bounds(new_map)
	#send_socket("New progressive goal at %s" % current_goal)
	send_goal_event("I need to head %s!")
	#print "New progressive goal at (%s, %s) within bounding box [(%s, %s), (%s, %s)]" % (current_goal[0], current_goal[1], bounding_rect.tlX, bounding_rect.tlY, bounding_rect.brX, bounding_rect.brY)
	update_goal_paths()
		

goal_prob = [[1,0.157],[2,0.157],[3,0.157],[4,0.157],[5,0.149],[6,0.144],[7,0.08]]

min_goal_distance = -1
map_bounds = []

prenormal_prob = []

def update_goal_paths(chaos=True,new_map=False):
	MAX_GOAL_WEIGHT =0.5 # maximum pull a goal can have

	global prenormal_prob, GOAL_PERIOD, map_bounds, goal_prob, min_goal_distance, pathfind_prob
	map_bounds = [vba.read_memory_at(0xd369)*2,vba.read_memory_at(0xd368)*2]
	map_bounds = [map_bounds[0]+GOAL_EXTENSION, map_bounds[1]+GOAL_EXTENSION]

	if DISTRIBUTION == Distributions.NoChaosGoals:
		chaos = False


	if not chaos:
		if(current_goal[0] < this_x):
			goal_prob[0] = [1,1]
			goal_prob[1] = [2,0]
		elif(current_goal[0] > this_x):
			goal_prob[1] = [2,1]
			goal_prob[0] = [1,0]
		if(current_goal[1] < this_y):
			goal_prob[3] = [4,1]
			goal_prob[2] = [3,0]
		elif(current_goal[1] > this_y):
			goal_prob[2] = [3,1]
			goal_prob[3] = [4,0]

		if(current_goal[0] == this_x):
			goal_prob[0] = [1,0]
			goal_prob[1] = [2,0]

		if(current_goal[1] == this_y):
			goal_prob[3] = [4,0]
			goal_prob[2] = [3,0]

		if(current_goal[0] == this_x and current_goal[1] == this_y):
			GOAL_PERIOD -= 1
			if GOAL_PERIOD <= 0:
				GOAL_PERIOD = 1	
			set_new_goal()


		
		dist_x = (current_goal[0] - this_x)/(float(map_bounds[0]-GOAL_EXTENSION))

		dist_y = (current_goal[1] - this_y)/(float(map_bounds[1]-GOAL_EXTENSION))

	else:

		dist_x = (current_goal[0] - this_x)/(float(map_bounds[0]-GOAL_EXTENSION))

		if(dist_x > 0):
			dist_x = 1-dist_x
		else:
			dist_x = -1-(dist_x)

		
		# DEBUG: uncomment to ignore collision weights	
		pathfind_prob = copy.deepcopy(pathfind_default)

		
		# if neg, player is right of goal
		if(dist_x > 0):
			goal_prob[1] = [2,pathfind_prob[1][1]*dist_x]
		if(dist_x <= 0):
			goal_prob[0] = [1,pathfind_prob[0][1]*abs(dist_x)]


		dist_y = (current_goal[1] - this_y)/(float(map_bounds[1]-GOAL_EXTENSION))
		if(dist_y > 0):
			dist_y = 1-dist_y
		else:
			dist_y = -1-(dist_y)
			
		# if neg, player is below goal
		if(dist_y > 0):
			goal_prob[2] = [3,pathfind_prob[2][1]*dist_y]
		elif(dist_y <=0):
			goal_prob[3] = [4,pathfind_prob[3][1]*abs(dist_y)]
		

		# adjust corresponding components
		if(dist_x > 0):
		
			goal_prob[0] = [1,abs((0.5-abs(dist_x)) * pathfind_prob[0][1])] 
		else:
			goal_prob[1] = [2,abs((0.5-abs(dist_x)) * pathfind_prob[1][1])]

		if(dist_y > 0):
			goal_prob[3] = [4,abs((0.5-abs(dist_y)) * pathfind_prob[3][1])]
	 	else:
			goal_prob[2] = [3,(abs(0.5-abs(dist_y)) * pathfind_prob[2][1])]	

	prenormal_prob = copy.deepcopy(goal_prob)

	# now normalise the probabilities to fit
	NORMALIZE_DPAD_TO = 0.627
	
	prob_sum = 0	
	for i in xrange(0,4):
		#prob_sum += abs(goal_prob[i][1])
		prob_sum += goal_prob[i][1]

		# if anything is at 0, set to 0.01 so it's still represented
		if DISTRIBUTION != Distributions.NoChaosGoals:
			if(goal_prob[i][1] <= 0):
				goal_prob[i][1] = 0.01

	if (prob_sum == 0):
		prob_sum = 1

	prob_fix = NORMALIZE_DPAD_TO / prob_sum

	#print "pre fix: %s" % goal_prob
	#print "prob fix %s" % prob_fix
	for i in xrange(0,4):
		goal_prob[i][1] = goal_prob[i][1]*prob_fix
		#
		

	distance = (abs(dist_x) + abs(dist_y))
	#print "At (%s,%s) going to %s. Closeness: %s" % (this_x,this_y,current_goal,distance)
	
	if(min_goal_distance < 0 or (distance < min_goal_distance)):
		min_goal_distance = distance


	#print "Updated goal distribution: %s" % goal_prob
	if min_goal_distance == 2:
		#print "Goal reached! Setting new one"	
		GOAL_PERIOD -= 1
		if GOAL_PERIOD <= 0:
			GOAL_PERIOD = 1	
		set_new_goal()


last_key, last_x, last_y, this_x, this_y  = None, None, None, None, None
def update_pathfinds():
	global pathfind_prob
	if last_is_in_battle or last_key > 4:
		return

	else:
		"""
		if ((last_x == this_x) and (last_y == this_y)):

				pathfind_prob[last_key-1][1] = pathfind_prob[last_key-1][1] - PATH_PENALTY
				#print "Collision. Probability of %s reduced" % friendly_maps[last_key]

				# opposite direction gets 20% boost, perpendiculars get 40% each

				if(last_key == 1): #left
					#pathfind_prob[1][1] = pathfind_prob[1][1] + (PATH_PENALTY/5)
					pathfind_prob[2][1] = pathfind_prob[2][1] + ((PATH_PENALTY/2))
					pathfind_prob[3][1] = pathfind_prob[3][1] + ((PATH_PENALTY/2))

				if(last_key == 2): #right
					#pathfind_prob[0][1] = pathfind_prob[0][1] + (PATH_PENALTY/5)
					pathfind_prob[2][1] = pathfind_prob[2][1] + ((PATH_PENALTY/2))
					pathfind_prob[3][1] = pathfind_prob[3][1] + ((PATH_PENALTY/2))

				if(last_key == 3): #down
					#pathfind_prob[3][1] = pathfind_prob[3][1] + (PATH_PENALTY/5)
					pathfind_prob[0][1] = pathfind_prob[0][1] + ((PATH_PENALTY/2))
					pathfind_prob[1][1] = pathfind_prob[1][1] + ((PATH_PENALTY/2))

				if(last_key == 4): #up
					#pathfind_prob[2][1] = pathfind_prob[2][1] + (PATH_PENALTY/5)
					pathfind_prob[0][1] = pathfind_prob[0][1] + ((PATH_PENALTY/2))
					pathfind_prob[1][1] = pathfind_prob[1][1] + ((PATH_PENALTY/2))

				# don't let probabilities drop below zero
				for i in xrange(len(pathfind_prob)):
					if pathfind_prob[i][1] < 0:
						pathfind_prob[i][1] = 0
				#print "New probabilities: %s" % pathfind_prob
				#print "Last goal prob: %s" % goal_prob

		else:
		"""
		send_map_event(vba.read_memory_at(0xd35e))

# returns a key based on the current strategy
def get_key():
	if(DISTRIBUTION == Distributions.LessStart):
		return random.choice(less_start)

 	elif(DISTRIBUTION == Distributions.OverallTwitch):
		return random_distr(twitch_prob)

	elif(DISTRIBUTION == Distributions.DumbPathFind):
		if not is_in_battle():
			return random_distr(pathfind_prob)
		else:
			return random_distr(twitch_prob)

	elif(DISTRIBUTION == Distributions.RandomGoal or DISTRIBUTION == Distributions.ProgressiveGoal or DISTRIBUTION == Distributions.NoChaosGoals):
		if not is_in_battle() and not menu_on:
			return random_distr(goal_prob)		
		else:
			return random_distr(twitch_prob)


# stolen. used for twitch distribution
def random_distr(l):
    assert l # don't accept empty lists
    r = random.uniform(0, 1)
    s = 0
    for i in xrange(len(l)):

        item, prob = l[i]
        s += prob
        if s >= r:
		return item
    return item


def printMemoryAt(loc,title):
	try:
		print "DEBUG | %s | %s" % (title, vba.read_memory_at(loc))
	except:
		pass


def get_text(chars=chars, offset=0, bounds=2000):
        """
        Returns alphanumeric text on the screen.

        Other characters will not be shown.
        """
        output = ""
        tiles = vba.memory[0xc103 + offset:0xc103 + offset + bounds]
        for each in tiles:
            if each in chars.keys():
                thing = chars[each]
                acceptable = False

                if len(thing) == 2:
                    portion = thing[1:]
                else:
                    portion = thing

                if portion in string.printable:
                    acceptable = True

                if acceptable:
                    output += thing
        # remove extra whitespace
        output = re.sub(" +", " ", output)
        output = output.strip()

        return output


stdscr = None
def init_curses():
	global stdscr
	stdscr = curses.initscr()
	curses.noecho()
	curses.start_color()
	curses.init_pair(1, curses.COLOR_BLUE, curses.COLOR_YELLOW) 	# title colors
	curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_RED) 	# section head 
	curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_BLACK) # body	


def update_curses():
	stdscr.erase()
	try:
		stdscr.addstr(0,0, "pyPoke 2.0 - Gotta run aimlessly round em all!", curses.color_pair(1) | curses.A_BOLD)
		moves = "Move %s" % key_count
		x_pos = stdscr.getmaxyx()[1] - len(moves)
		stdscr.addstr(0,x_pos, "%s" % moves, curses.color_pair(1) | curses.A_BOLD)

	#	stdscr.addstr(2,2, "Probabilities (goal-adjusted)", curses.color_pair(2) | curses.A_BOLD)
	#	stdscr.addstr(3,7,"%s" %  prenormal_prob, curses.color_pair(3))

		
		stdscr.addstr(2,2, "extend dir, start key", curses.color_pair(2) | curses.A_BOLD)
		stdscr.addstr(3,7,"%s | %s" % (bounding_rect.extend_dir, bounding_rect.start_key) , curses.color_pair(3))

		stdscr.addstr(5,2, "Probabilities (GA normalised)", curses.color_pair(2) | curses.A_BOLD)
		stdscr.addstr(6,7,"%s" %  goal_prob, curses.color_pair(3))

		stdscr.addstr(8,2, "Goal length", curses.color_pair(2) | curses.A_BOLD)
		stdscr.addstr(9,7,"%s" % GOAL_PERIOD, curses.color_pair(3))

		stdscr.addstr(8,30, "Current goal", curses.color_pair(2) | curses.A_BOLD)
		stdscr.addstr(9,30,"%s (expires in %s moves)" %  (current_goal, (GOAL_PERIOD-(no_battle_key_counts%GOAL_PERIOD))) , curses.color_pair(3))

		stdscr.addstr(8,70, "Current location", curses.color_pair(2) | curses.A_BOLD)
		stdscr.addstr(9,70,"[%s,%s]" % (this_x, this_y)  , curses.color_pair(3))

		stdscr.addstr(10,2, "Next goal weights (U/R/D/L)", curses.color_pair(2) | curses.A_BOLD)
		stdscr.addstr(11,7,"%s" % bounding_rect.dir_weights, curses.color_pair(3))
	
		stdscr.addstr(10,35,"Meta goal", curses.color_pair(2) | curses.A_BOLD)
		stdscr.addstr(11,35,"%s (expires in %s moves)" % (meta_goal,(meta_goal_turns) - (meta_key_counts%(meta_goal_turns))), curses.color_pair(3))	
		
		stdscr.addstr(10,75,"Meta active?", curses.color_pair(2) | curses.A_BOLD)
		stdscr.addstr(11,75,"%s" % meta_active, curses.color_pair(3))	
	

		printMapState()
	except:
		raise
		pass

	stdscr.refresh() 


wss =[]
def send_socket(message):
    for ws in wss:
        if not ws.ws_connection.stream.socket:
            print "Web socket does not exist anymore!!!"
            wss.remove(ws)
        else:
            ws.write_message(message)


def printMapState():
	if not map_bounds:
		return

	try:
		coverage = int(math.floor(bounding_rect.area_of_map_bounds(map_bounds)*100))
		out = ""
		map_y_offset = 13
		stdscr.addstr(map_y_offset,2, "Map (%s%% explored)" % coverage, curses.color_pair(2) | curses.A_BOLD)
		for y in xrange(map_bounds[1]+1):
			for x in xrange(map_bounds[0]+1):
				if [x,y] == current_goal:
					out = "G"
				elif ((x < 2 or x > map_bounds[0]-2) or (y < 2 or y > map_bounds[1]-2)):
					out = "="		
				elif [x,y] == [this_x, this_y]:
					out = "P"
				elif bounding_rect.point_in_rect(x,y):
					out = "X"
				else:
					out = "."		
				stdscr.addstr(map_y_offset+y+1,x+5,"%s" % out, curses.color_pair(3))
	except:
		#raise
		pass
key_count = 0


time_count = -1
time_origin = -1
# do we have a file with the last execution time of this game?
def init_clock():
	global time_count, time_origin, key_count
	try:
			f = open("pyclock")
			line = f.readline().split("|")
			time_count = int(line[0])
			key_count = int(line[1])

	except:
			time_count = 0
	time_origin = time.time()
	time_count = float(time_count)

def write_time():
	time_calc = time_count + (int(time.time()) - time_origin)
	f = open("pyclock","w")
	f.write("%s|%s\n" % (int(time_calc), key_count))

menu_on = False
last_tileset = -1
meta_goal_turns = DEFAULT_GOAL_PERIOD * META_MULTIPLIER

#event flags
event_isInBattle = False
event_lastMap = -1
event_lastTileset = -1


def send_event(event):
	send_socket("EVENT|%s|%s" % (event, key_count))

def eventDetectPump():
	global event_isInBattle, event_lastMap, event_lastTileset

	# battle detection
	if(is_in_battle()):
		if not event_isInBattle:
			enemy_level = vba.read_memory_at(0xcff3)
			enemy_poke = vba.read_memory_at(0xcfe5)
			send_event("Entered battle with a level %s %s" % (enemy_level, pokedex[enemy_poke]))
			event_isInBattle = True

	if(not is_in_battle() and event_isInBattle):
		event_isInBattle = False

	# map change detection
	this_tileset = vba.read_memory_at(0xd367)
	this_map = vba.read_memory_at(0xd35e)

	#if(this_tileset != event_lastTileset):
	#	send_event("Moved from tile %s to %s" % (event_lastTileset, this_tileset))
	#	event_lastTileset = this_tileset

	if(this_map != event_lastMap and this_map != 3):
		send_event("Moved from %s to %s" % (maps[event_lastMap], maps[this_map]))
		event_lastMap = this_map

def send_goal_event(why):
	# in which compass point does this point lie?

	direct = "somewhere"
	if(current_goal[0] < this_x and current_goal[1] < this_y):
		direct = "northwest"
	elif(current_goal[0] > this_x and current_goal[1] < this_y):
		direct = "northeast"
	elif(current_goal[0] < this_x and current_goal[1] > this_y):
		direct = "southwest"
	elif(current_goal[0] > this_x and current_goal[1] > this_y):
		direct = "southeast"
	elif(current_goal[0] == this_x and current_goal[1] < this_y):
		direct = "north"
	elif(current_goal[0] > this_x and current_goal[1] == this_y):
		direct = "east"
	elif(current_goal[0] == this_x and current_goal[1] > this_y):
		direct = "south"
	elif(current_goal[0] < this_x and current_goal[1] == this_y):
		direct = "west"

	why = why % direct


	send_socket("GOAL|%s|%s|%s|%s" % (current_goal[0],current_goal[1],why,key_count)) 

def send_map_event(map_id):
	# format:
	# thisX, thisY, mapId, width, height, visitTLX, visitTlY, visitBrX, visitBrY, key_count
	send_socket("MAP|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s" % (this_x,this_y,maps[map_id], map_bounds[0], map_bounds[1], bounding_rect.tlX, bounding_rect.tlY, bounding_rect.brX, bounding_rect.brY, key_count))


meta_key_counts = 0
screen_text = ""
map_change_key = -1
last_dir_key = -1

def doPokeEmu():
	init_clock()
	init_curses()
	global last_dir_key, map_change_key, screen_text, meta_key_counts, no_battle_key_counts, key_count
	no_battle_key_counts = 0
	global meta_goal_turns, meta_active, meta_goal, menu_on, last_tileset, GOAL_PERIOD, last_is_in_battle, last_key, last_x, last_y, this_x, this_y, bounding_rect, map_bounds
	last_map = -1
	last_goal_distance = -1
	last_tileset = -1
	session_key_count = 0
	while True:
			eventDetectPump()

			is_battle = is_in_battle()
			if(is_battle and not last_is_in_battle):
				send_socket("[%s]: Entered battle!" % key_count)
				#print "[%s]: Entered battle!" % key_count
				last_is_in_battle = True

			if(not is_battle and last_is_in_battle):
				#print "[%s]: Battle ended!" % key_count
				send_socket("[%s]: Battle ended!" % key_count)
				last_is_in_battle = False
			
			key_count += 1
			key = get_key()
#			print "Key %s | %s " % (key_count, friendly_maps[key])

			last_key = key
			last_x = vba.read_memory_at(0xd362) + (GOAL_EXTENSION/2)
			last_y = vba.read_memory_at(0xd361) + (GOAL_EXTENSION/2)

			vba.press(vba.button_masks[key_maps[key]],5,20)

			if last_key < 5:
				last_dir_key = last_key
			
			this_x = vba.read_memory_at(0xd362)
			this_y = vba.read_memory_at(0xd361)
			
			# we extend the map bounds to set goals outside the bounds so map the player x,y to this virtual space
			this_x = this_x + (GOAL_EXTENSION/2)
			this_y = this_y + (GOAL_EXTENSION/2)
			

			done_map = False
			if(DISTRIBUTION == Distributions.RandomGoal or DISTRIBUTION == Distributions.ProgressiveGoal or DISTRIBUTION == Distributions.NoChaosGoals):
				if session_key_count > 1 and not menu_on and not is_in_battle():
					if current_goal == [0,0]:
						set_new_goal()
					update_goal_paths()
				this_map = vba.read_memory_at(0xd35e)
				this_tileset = vba.read_memory_at(0xd367)			
				
				"""
				if((abs(this_x-last_x) > 2) or (abs(this_y-last_y) > 2)):
					GOAL_PERIOD = DEFAULT_GOAL_PERIOD
					meta_goal_turns = GOAL_PERIOD * META_MULTIPLIER
					meta_goal = -1
					meta_active = False
					map_change_key = last_dir_key
					done_map = True
					bounding_rect = Rectangle(this_x-2, this_y-2,this_x+2,this_y+2)
					map_bounds = [vba.read_memory_at(0xd369)*2,vba.read_memory_at(0xd368)*2]
					map_bounds = [map_bounds[0]+GOAL_EXTENSION, map_bounds[1]+GOAL_EXTENSION]
					
					set_new_goal(False,True)	
				"""
				if(last_map != this_map and this_map != 3 and not done_map):
					#print "Moved from map %s to %s - setting new goal." % (last_map, this_map)
					GOAL_PERIOD = DEFAULT_GOAL_PERIOD
					meta_goal_turns = GOAL_PERIOD * META_MULTIPLIER
					meta_goal = -1
					meta_active = False
					last_map = this_map
					map_change_key = last_dir_key
					bounding_rect = Rectangle(this_x-1, this_y-1,this_x+1,this_y+1)
					map_bounds = [vba.read_memory_at(0xd369)*2,vba.read_memory_at(0xd368)*2]
					map_bounds = [map_bounds[0]+GOAL_EXTENSION, map_bounds[1]+GOAL_EXTENSION]
					pathfind_prob = copy.deepcopy(pathfind_default) #reset probs to default
					if(map_bounds[0] != GOAL_EXTENSION):
						set_new_goal(False,True)	

				if(last_tileset != this_tileset and not done_map):
					#print "Changed from tileset %s to %s - new goal!" % (last_tileset, this_tileset)
					GOAL_PERIOD = DEFAULT_GOAL_PERIOD
					meta_goal_turns = GOAL_PERIOD * META_MULTIPLIER
					meta_goal = -1
					meta_active = False
					last_tileset = this_tileset
					map_change_key = last_dir_key
					bounding_rect = Rectangle(this_x-1, this_y-1, this_x+1, this_y+1)
					map_bounds = [vba.read_memory_at(0xd369)*2,vba.read_memory_at(0xd368)*2]
					map_bounds = [map_bounds[0]+GOAL_EXTENSION, map_bounds[1]+GOAL_EXTENSION]
					pathfind_prob = copy.deepcopy(pathfind_default) #reset probs to default
					if(map_bounds[0] != GOAL_EXTENSION):
						set_new_goal(False,True)

				if(session_key_count > 1 and not is_in_battle() and not menu_on):
					no_battle_key_counts += 1
					meta_key_counts += 1

				set_already = False

				if(map_bounds[0] != GOAL_EXTENSION):
					if(bounding_rect.area_of_map_bounds(map_bounds)  > meta_goal):
						meta_active = False
				else:
					session_key_count = -1
					
				if(meta_key_counts % meta_goal_turns == 0 and not is_in_battle() and not menu_on):
					meta_key_counts = 0	
					meta_goal_turns = (GOAL_PERIOD * META_MULTIPLIER)
					if(map_bounds[0] != GOAL_EXTENSION and bounding_rect.area_of_map_bounds(map_bounds)  > meta_goal):
						meta_goal = bounding_rect.area_of_map_bounds(map_bounds) 
						meta_active = False
					else:
						if(map_bounds[0] != GOAL_EXTENSION):
							meta_active = True
							#set_new_goal(True)
							#set_already = True

				if(no_battle_key_counts % GOAL_PERIOD == 0 and not set_already and not is_in_battle() and not menu_on):
					if(last_goal_distance == -1):
						last_goal_distance = min_goal_distance
					else:
						if(min_goal_distance == last_goal_distance):
							#print "Last goal was not met. Assigning new one"
							GOAL_PERIOD += 1
							set_new_goal()
							last_goal_distance = -1 

						else:
							last_goal_distance = min_goal_distance
							#print "Closer to goal - now %s away" % last_goal_distance

			#printMemoryAt(0xd358,"map number")
			#printMemoryAt(0xd369,"map width")
			#printMemoryAt(0xd368,"map height")
			#printMemoryAt(0xd362,"current x block")
			#printMemoryAt(0xd361,"current y block")


			screen_text = get_text()
			if(len(regex.findall(screen_text)) > 1):
				menu_on = True
			else:
				menu_on = False


			if session_key_count > 1:
				bounding_rect.extend_to_point(this_x, this_y)
			if session_key_count > 1 and not menu_on: #arbitrary wait for game to start
				update_pathfinds()

			session_key_count += 1
			update_curses()

RUN_AS_SERVER = True
if __name__ == "__main__":
	import os
	os.system("clear")

	sr = random.SystemRandom()
	sys.modules["random"] = sr
	import random


	print "Pokerandy - Gotta run aimlessly round em all!"
	print "============================================"
	print "Handing control over to you. Press F12 when you want the simulation to take over"
	vba.run()

	try:
		threading.Thread(target=doPokeEmu).start()
		if(RUN_AS_SERVER):
			app = Application()
    			app.listen(7000)
    			tornado.ioloop.IOLoop.instance().start()
	
	except KeyboardInterrupt:
		curses.endwin()
