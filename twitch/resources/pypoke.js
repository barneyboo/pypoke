// pypoke 1.0
// Copyright Luke Hutton 2014

function renderMapCanvas() {
	
	var widthScale = parseFloat(216) / mapWidth;
	var heightScale = parseFloat(416) / mapHeight;

	var context = document.getElementById("map_canvas").getContext("2d");
	context.save();	
	if(widthScale < heightScale) 
		context.scale(widthScale,widthScale);		

	else
		context.scale(heightScale,heightScale);
	
	context.clearRect(0,0,216,416);
	//context.scale(3,3);

	// outer

	context.fillStyle = "rgba(73,73,73,0.35)"
	context.save();
	 context.beginPath();
	 context.strokeStyle = 'black';
	 context.lineWidth = 5;
	 context.shadowBlur = 15
	 context.shadowColor = 'black';
	 context.shadowOffsetX = 0;
	 context.shadowOffsetY = 0;
    	 context.fillRect(0,0,mapWidth,mapHeight);	

//	context.restore();

	// visitable
	context.fillStyle = "rgba(255,255,255,0.8)"
	context.fillRect(2,2,mapWidth-4,mapHeight-4);

	
	// visited
	var visitWidth = parseFloat(visitBrX) - parseFloat(visitTlX);
	var visitHeight = parseFloat(visitBrY) - parseFloat(visitTlY);
	var my_gradient=context.createLinearGradient(visitTlX,visitTlY,visitTlX,visitBrY);
	my_gradient.addColorStop(0,"#ebcf4c");
//	my_gradient.addColorStop(0.5,"#fff45a");
	my_gradient.addColorStop(0.5,"rgba(255,244,90,0.6)");
	my_gradient.addColorStop(1,"#ebcf4c");
	

	context.fillStyle=my_gradient;
	//context.fillStyle = "rgb(241,212,78)"
	recX = parseFloat(visitTlX);
	recY = parseFloat(visitTlY);
	context.fillRect(recX,recY,visitWidth,visitHeight);	
	
	context.restore();

	// position circle
	context.beginPath();
	arcX = parseFloat(thisX);
	arcY = parseFloat(thisY);
	context.arc(arcX, arcY, 0.5, 0, 2*Math.PI, false);
	context.fillStyle = "rgb(244,0,110)"
	context.fill();

	// goal circle
	context.beginPath();
	begX = parseFloat(goalX);
	begY = parseFloat(goalY);
	context.arc(begX, begY, 0.5, 0, 2*Math.PI, false);
	context.fillStyle = "rgb(0,167,244)"
	context.fill();
	context.restore();
}

var thisX;
var thisY;
var mapWidth;
var mapHeight;
var visitTlX;
var visitTlY;
var visitBrX;
var visitBrY;
var goalX;
var goalY;

$(document).ready(function() {
	//window.resizeTo(1024,576);
	window.moveTo(0,0);

	$(".location_slug").hide();

	var socket = new WebSocket("ws://localhost:7000/socket")
	socket.onmessage = function (evt) 
     { 
     	var msg = evt.data.split("|");

     	if(msg[0] == "STATEDUMP") {
     		var state = jQuery.parseJSON(msg[1]);
     		 	$('.clock').stopwatch({startTime: state.rtc*1000, format:"{Days} {hours} {minutes} {seconds}"}).stopwatch('start');
     	}

     	if(msg[0] == "EVENT") {
			if($(".activity_unit").length > 5)
				$(".activity_unit:nth-last-child(2)").remove()
	
	     	$(".activity_top").removeClass("activity_top",500);
	     	var newBlock = $(".reference_unit").clone();
	     	newBlock.removeClass("reference_unit");
			newBlock.addClass("activity_top");
	     	newBlock.find("p:first").text(msg[1]);
	     	newBlock.find(".activity_time p").first().attr("data-livestamp",moment().unix())
	     	newBlock.insertAfter($(".section_top"));
			newBlock.fadeIn();
		}

		if(msg[0] == "GOAL") {
			$(".slug_bottom").text(msg[3]);
			goalX = msg[1];
			goalY = msg[2];
			renderMapCanvas();
		}

		if(msg[0] == "MAP") {
			thisX = msg[1];
			thisY = msg[2];
			mapWidth = parseFloat(msg[4]);
			mapHeight = parseFloat(msg[5])+1;
			visitTlX = parseFloat(msg[6]);
			visitTlY = parseFloat(msg[7]);
			visitBrX = parseFloat(msg[8]);
			visitBrY = parseFloat(msg[9]);
			$(".slug_top").text(msg[3]);
			$(".location_slug").fadeIn();
			console.log(msg)
			renderMapCanvas();
		}
     };
});
